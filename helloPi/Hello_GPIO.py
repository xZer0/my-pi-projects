#!/usr/bin/python

import sys, os, time
import RPi.GPIO as GPIO

GPIO.setup(11, GPIO.IN)     #GPIO0
GPIO.setup(12, GPIO.IN)     #GPIO1
GPIO.setup(13, GPIO.OUT)    #GPIO2
GPIO.setup(15, GPIO.OUT)    #GPIO3
    
while 1:
    # Off LED
    print "LED Off"
    GPIO.output(13, True)
    time.sleep(0.3)
    
    # On LED
    print "LED On"
    GPIO.output(13, False)
    time.sleep(0.7)