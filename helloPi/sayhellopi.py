#!/usr/bin/python

import sys, os, time

def count(n):   
    for i in range(n):
        for j in range(i+1):
            print j,
        print
        time.sleep(i+1)
        
    print "Ready!!!"
    
def main():
    count(10)
    print "Hello Raspberry Pi"
    
if __name__ == "__main__":
    main()